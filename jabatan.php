<!-- <?php include 'connect.php'; ?> -->
<?php include 'header.php'; ?>

    <div class = "container">
    <h1 class="mt-3 mb-3">Tabel Jabatan</h1>
        <a href="formJabatan.php" class="btn btn-sm btn-success mb-3">Tambah</a>
        <table class="table">
            <thead class="table-secondary">
                <tr>
                    <th>Jabatan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                
                <?php
                    $sql = 'SELECT * FROM jabatan';

                    $query = mysqli_query($conn, $sql);

                    while ($row = mysqli_fetch_object($query)) {
                ?>
                
                <tr>

                    <td><?php echo $row->jabatan; ?></td>
  
                    <td>
                        <a href="formjabatan.php?id_jabatan=<?php echo $row->id_jabatan; ?>" class="btn btn-sm btn-warning">Ubah</a>

                        <a href="deletejabatan.php?id_jabatan=<?php echo $row-> id_jabatan; ?>" class="btn btn-sm btn-outline-danger"
                        onclick = "return confirm('Apakah Anda Yakin Menghapus Data?');">Hapus</a>
                    </td>
                </tr>

                <?php
                    } if (!mysqli_num_rows($query)) {
                        echo '<tr><td colspan="6" class="text-center">Tidak ada data.</td></tr>';
                    }
                ?>

            </tbody>
        </table>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>  
</body>
</html>